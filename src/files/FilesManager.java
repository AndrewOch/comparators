package files;

import java.io.File;
import java.io.IOException;

public class FilesManager {


    public static void main(String[] args) {
        FilesManager filesManager = new FilesManager();
        //filesManager.createFile("C:\\Users\\007\\Desktop\\Vigvamcev\\vigvamcev-fun\\fun.txt");
        filesManager.seeContent("C:\\Users\\007\\Desktop\\Vigvamcev\\vigvamcev-fun", 0);
    }

    void createFile(String path) {
        File file = new File(path);
        if (file.exists()) {

            if (file.isFile()) {
                if (path.lastIndexOf('.') != -1 && path.lastIndexOf('.') != 0) {
                    file = new File(path.substring(0, path.lastIndexOf('.')));
                    file.mkdir();
                }

            } else if (file.isDirectory()) {
                file = new File(file.getPath() + "\\" + path);
                try {
                    file.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        } else {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    void seeContent(String path, int nestingDepth) {
        File dir = new File(path);
        if (dir.isDirectory()) {
            for (File item : dir.listFiles()) {
                if (item.isDirectory()) {
                    for (int i = 0; i < nestingDepth; i++) {
                        System.out.print("      ");
                    }
                    System.out.println("Directory - " + item.getName());
                    if (nestingDepth < 2) {

                        seeContent(item.getPath(), nestingDepth + 1);
                    }
                }
                if (item.isFile()) {
                    for (int i = 0; i < nestingDepth; i++) {
                        System.out.print("      ");
                    }
                    System.out.println("File     -- " + item.getName());
                }
            }
        }
    }
}
