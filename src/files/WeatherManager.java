package files;

import java.io.*;
import java.util.ArrayList;

public class WeatherManager {

    //Добро пожаловать в программу обработки погоды в городах Швейцарии
    //Сегодня будем обрабатывать .csv файл погоды В городе Базель за прошлую неделю

    public static void main(String[] args) {
        WeatherManager weatherManager = new WeatherManager();

        //Указываем путь к файлу который нужно обработать
        File file = new File("C:\\Users\\007\\Desktop\\Vigvamcev\\comparators\\weatherFiles\\dataexport_20210320T064822.csv");

        weatherManager.parseWeather(file);
    }


    File parseWeather(File file) {

        //Данные о погоде будем хранить в ArrayList Погод (см. класс Weather)
        ArrayList<Weather> weathers = new ArrayList<>();

        //Так как надо записывать всё в файл, указываем ему путь (в ту же папку, что и файл, который обрабатываем)
        File result = new File(file.getParent() + "\\weather_data.txt");

        try {
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            //Отступаем 10 строк, в файле погоды в них лежит разная ненужная информация
            for (int i = 0; i < 10; i++) {
                bufferedReader.readLine();
            }

            String line = null;

            //Далее читаем все строки до конца файла и собираем из них объекты Weather
            //Пример строки:
            //20210320T2100,0.47052866,61.0,7.8625183,74.054596
            //Делим её на части:
            //2021 03 20 21 0.47052866 61.0 7.8625183 74.054596
            //И кладём все параметры в конструктор Weather, добавляем в ArrayList
            while ((line = bufferedReader.readLine()) != null) {

                Integer year = Integer.valueOf(line.substring(0, 4));
                line = line.substring(4);

                Integer month = Integer.valueOf(line.substring(0, 2));
                line = line.substring(2);

                Integer day = Integer.valueOf(line.substring(0, 2));
                line = line.substring(2);
                Integer hour = Integer.valueOf(line.substring(1, 3));
                line = line.substring(6);

                Float temperature = Float.valueOf(line.substring(0, line.indexOf(',')));
                line = line.substring(line.indexOf(',') + 1);
                Float relativeHumidity = Float.valueOf(line.substring(0, line.indexOf(',')));
                line = line.substring(line.indexOf(',') + 1);

                Float windSpeed = Float.valueOf(line.substring(0, line.indexOf(',')));
                line = line.substring(line.indexOf(',') + 1);

                Float windDirection = Float.valueOf(line);

                Weather weather = new Weather(year, month, day, hour, temperature, relativeHumidity, windSpeed, windDirection);
                weathers.add(weather);
            }

            //Закрываем чтение и переходим ко 2 фазе
            bufferedReader.close();
            fileReader.close();

            //Создаём файл по пути, который мы указали в начале метода
            result.createNewFile();
            FileWriter fileWriter = new FileWriter(result);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            //Теперь будем записывать в созданный файл всё что нам надо

            //Нахождение средних значений засунул в отдельные методы, там всё просто
            bufferedWriter.write("Средняя температура    : " + averageTemperature(weathers) + "\n");
            bufferedWriter.write("Средняя влажность      : " + averageHumidity(weathers) + "\n");
            bufferedWriter.write("Средняя скорость ветра : " + averageWindSpeed(weathers) + "\n\n");

            //Методы наибольших значений возвращают индекс в массиве погоды с рекордным значением чего-то
            //Мы можем взять нужную погоду по индексу и вызвать Геттеры
            Weather weather = weathers.get(indexOfHighestTemperature(weathers));
            bufferedWriter.write("Самая высокая температура : " + weather.getTemperature() + " - " + weather.getHour() + ":00 " + weather.getDay() + "." + weather.getMonth() + "." + weather.getYear() + "\n");
            weather = weathers.get(indexOfMinimumHumidity(weathers));
            bufferedWriter.write("Самая низкая влажность    : " + weather.getRelativeHumidity() + " - " + weather.getHour() + ":00 " + weather.getDay() + "." + weather.getMonth() + "." + weather.getYear() + "\n");
            weather = weathers.get(indexOfMostPowerfulWind(weathers));
            bufferedWriter.write("Самый сильный ветер       : " + weather.getWindSpeed() + " - " + weather.getHour() + ":00 " + weather.getDay() + "." + weather.getMonth() + "." + weather.getYear() + "\n\n");

            //См. метод
            bufferedWriter.write("Самое частое направление ветра : " + findMostCommonWindDirection(weathers));

            bufferedWriter.close();
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }


    Float averageTemperature(ArrayList<Weather> weathers) {
        Float average = 0f;
        for (Weather weather : weathers) {
            average += weather.temperature;
        }
        return average / (weathers.size());
    }

    Float averageHumidity(ArrayList<Weather> weathers) {
        Float average = 0f;
        for (Weather weather : weathers) {
            average += weather.relativeHumidity;
        }
        return average / (weathers.size());
    }

    Float averageWindSpeed(ArrayList<Weather> weathers) {
        Float average = 0f;
        for (Weather weather : weathers) {
            average += weather.windSpeed;
        }
        return average / (weathers.size());
    }

    int indexOfHighestTemperature(ArrayList<Weather> weathers) {
        int result = 0;
        Float record = weathers.get(0).getTemperature();
        for (int i = 0; i < weathers.size(); i++) {
            if (weathers.get(i).getTemperature() > record) {
                record = weathers.get(i).getTemperature();
                result = i;
            }
        }
        return result;
    }

    private int indexOfMinimumHumidity(ArrayList<Weather> weathers) {
        int result = 0;
        Float minimum = weathers.get(0).getRelativeHumidity();
        for (int i = 0; i < weathers.size(); i++) {
            if (weathers.get(i).getRelativeHumidity() < minimum) {
                minimum = weathers.get(i).getRelativeHumidity();
                result = i;
            }
        }
        return result;
    }


    private int indexOfMostPowerfulWind(ArrayList<Weather> weathers) {
        int result = 0;
        Float record = weathers.get(0).getWindSpeed();
        for (int i = 0; i < weathers.size(); i++) {
            if (weathers.get(i).getWindSpeed() > record) {
                record = weathers.get(i).getWindSpeed();
                result = i;
            }
        }
        return result;
    }

    private String findMostCommonWindDirection(ArrayList<Weather> weathers) {
        //Счётчики направлений ветра, в конце сравниваем какой больше и выводим
        int south = 0;
        int north = 0;
        int east = 0;
        int west = 0;

        //Проходимся по каждой погоде
        for (Weather weather : weathers) {
            float dir = weather.getWindDirection();

            //Сторону света определяем наименьшей разницей направления и 0,90,180,270,360 градусами

            //Сперва определим, к 0 ближе или к 360, так как и то и то относится к северу
            float min = dir - 0f;
            if (360f - dir < min) {
                min = 360f - dir;
            }

            //Находим минимальное среди 4 разниц
            min = Math.min(Math.min(min, Math.abs(90f - dir)), Math.min(Math.abs(180f - dir), Math.abs(270f - dir)));

            //Проверяем, какая сторона света, прибавляем счётчик
            if (min == Math.min(360f - dir, dir - 0f)) {
                north++;
            }
            if (min == Math.abs(90f - dir)) {
                east++;
            }
            if (min == Math.abs(180f - dir)) {
                south++;
            }
            if (min == Math.abs(270f - dir)) {
                west++;
            }
        }

        //Сравниваем счётчики, выводим строку
        if (north > south && north > east && north > west) {
            return "Север";
        }
        if (south > north && south > east && south > west) {
            return "Юг";
        }
        if (west > south && west > east && west > north) {
            return "Запад";
        }
        if (east > south && east > north && east > west) {
            return "Восток";
        }
        return null;
    }

}
