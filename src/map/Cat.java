package map;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Cat {

    String name;
    int age;
    Cat fatherCat;
    Cat motherCat;
    String color;

    public Cat(String name, int age, Cat fatherCat, Cat motherCat, String color) {
        this.name = name;
        this.age = age;
        this.fatherCat = fatherCat;
        this.motherCat = motherCat;
        this.color = color;
    }

    public static void main(String[] args) {
        //Map<String, List<Cat>> getNameCatMap(List<Cat> cats());
        //getNameCatMap.put();
    }

}
