package graphs;

import javafx.util.Pair;

import java.util.*;

public class Graph {
    ArrayDeque<Integer> arrayDeque = new ArrayDeque<>();

    public static void main(String[] args) {
        Graph graph = new Graph();
        //System.out.println(graph.breadthFirst(graph.matrix2(), 1));
        //System.out.println(graph.breadthFirst(graph.matrix1(), 1));
        System.out.println(graph.goDijkstra(graph.matrix3(), 0));
        System.out.println("Prima: " + graph.goPrima(graph.matrix3()));
        System.out.println("Kruskal: " + graph.goKruskal(graph.matrix3()));
        System.out.println("Kruskal: " + graph.goKruskal(graph.matrix4()));
        System.out.println("Kruskal: " + graph.goKruskal(graph.matrix5()));
        System.out.println("Kruskal: " + graph.goKruskal(graph.matrix6()));
    }

    int goKruskal(int[][] adjacencyMatrix) {
        int result = 0;
        TreeMap<Integer, List<Pair<Integer, Integer>>> map = new TreeMap<>();
        ArrayList<Pair<Integer, Integer>> arrayList;
        for (int i = 0; i < adjacencyMatrix.length; i++) {
            for (int j = i; j < adjacencyMatrix.length; j++) {
                if (adjacencyMatrix[i][j] > 0) {
                    if (!map.containsKey(adjacencyMatrix[i][j])) {
                        arrayList = new ArrayList<>();
                        arrayList.add(new Pair<>(i, j));
                        map.put(adjacencyMatrix[i][j], arrayList);
                    } else {

                        arrayList = (ArrayList<Pair<Integer, Integer>>) map.get(adjacencyMatrix[i][j]);
                        arrayList.add(new Pair<>(i, j));
                    }
                }
            }
        }
        System.out.println(map);
        boolean[][] ways = new boolean[adjacencyMatrix.length][adjacencyMatrix.length];

        while (!map.isEmpty()) {
            arrayList = (ArrayList<Pair<Integer, Integer>>) map.get(map.firstKey());
            for (Pair<Integer, Integer> item : arrayList) {
                if (!breadthFirst(ways, item.getKey()).contains(item.getValue().toString())) {
                    result += map.firstKey();
                    ways[item.getKey()][item.getValue()] = true;
                    ways[item.getValue()][item.getKey()] = true;

                }
            }
            map.remove(map.firstKey());
        }
        return result;
    }

    int goPrima(int[][] adjacencyMatrix) {
        int result = 0;
        int indexStart = 0;
        HashMap<Integer, ArrayList<Integer>> map = new HashMap<>();
        arrayDeque.offerFirst(0);
        while (arrayDeque.size() < adjacencyMatrix.length) {
            for (int i = 0; i < adjacencyMatrix.length; i++) {
                if (adjacencyMatrix[indexStart][i] > 0) {
                    if (!arrayDeque.contains(i)) {
                        if (map.containsKey(adjacencyMatrix[indexStart][i])) {

                            ArrayList<Integer> arrayList = map.get(adjacencyMatrix[indexStart][i]);
                            if (!arrayList.contains(i)) {
                                arrayList.add(i);
                            }
                        } else {
                            ArrayList<Integer> arrayList = new ArrayList<>();
                            arrayList.add(i);
                            map.put(adjacencyMatrix[indexStart][i], arrayList);
                        }
                    }
                }
            }

            boolean foundWay = false;
            TreeSet<Integer> set = new TreeSet<>(map.keySet());

            while (!foundWay) {

                Integer indexToDelete = null;
                ArrayList<Integer> arr;
                for (Integer item : set) {
                    if (!foundWay) {
                        arr = map.get(item);
                        for (int i = 0; i < arr.size(); i++) {
                            if (!arrayDeque.contains(arr.get(i))) {
                                arrayDeque.offerFirst(arr.get(i));
                                result += item;
                                indexStart = arr.get(i);
                                if (arr.size() == 1) {
                                    indexToDelete = item;
                                } else {
                                    arr.remove(i);
                                }
                                foundWay = true;
                                break;
                            }
                        }
                    }
                }
                if (indexToDelete != null) {
                    map.remove(indexToDelete);
                }
            }
        }
        arrayDeque.clear();
        return result;
    }


    Map<Integer, Integer> goDijkstra(int[][] adjacencyMatrix, int indexStart) {
        HashMap<Integer, Integer> result = new HashMap<>();
        result.put(indexStart, 0);
        arrayDeque.offerFirst(indexStart);
        while (!arrayDeque.isEmpty()) {
            for (int i = 0; i < adjacencyMatrix.length; i++) {
                if (adjacencyMatrix[indexStart][i] > 0) {
                    if (result.containsKey(i)) {
                        if (result.get(i) > result.get(indexStart) + adjacencyMatrix[indexStart][i]) {
                            result.replace(i, result.get(indexStart) + adjacencyMatrix[indexStart][i]);
                            if (!arrayDeque.contains(i)) {
                                arrayDeque.offerFirst(i);
                            }
                        }
                    } else {
                        result.put(i, result.get(indexStart) + adjacencyMatrix[indexStart][i]);
                        if (!arrayDeque.contains(i)) {
                            arrayDeque.offerFirst(i);
                        }
                    }
                }
            }
            if (!arrayDeque.isEmpty()) {
                indexStart = arrayDeque.pollLast();
            }
        }

        return result;
    }

    private int[][] matrix3() {
        int[][] matrix =
                {{0, 5, 0, 0, 3, 0},
                        {5, 0, 3, 7, 0, 0},
                        {0, 3, 0, 0, 0, 11},
                        {0, 7, 0, 0, 1, 42},
                        {3, 0, 0, 1, 0, 0},
                        {0, 0, 11, 42, 0, 0}};

        return matrix;
    }

    private int[][] matrix5() {
        int[][] matrix =
                {{0, 2, 2, 0, 0},
                        {2, 0, 2, 0, 3},
                        {2, 2, 0, 3, 0},
                        {0, 0, 3, 0, 3},
                        {0, 3, 0, 3, 0}};

        return matrix;
    }

    private int[][] matrix6() {
        int[][] matrix =
                {{0, 2, 0, 0, 0},
                        {2, 0, 4, 0, 0},
                        {0, 4, 0, 2, 0},
                        {0, 0, 2, 0, 2},
                        {0, 0, 0, 2, 0}};

        return matrix;
    }

    private int[][] matrix4() {
        int[][] matrix =
                {{0, 2, 2},
                        {2, 0, 2},
                        {2, 2, 0}};

        return matrix;
    }


    public String breadthFirst(boolean[][] adjacencyMatrix, int indexStart) {
        ArrayList<Integer> result = new ArrayList<>();

        arrayDeque.offerFirst(indexStart);
        while (!arrayDeque.isEmpty()) {
            for (int i = 0; i < adjacencyMatrix.length; i++) {
                if (adjacencyMatrix[indexStart][i] && !arrayDeque.contains(i) && !result.contains(i)) {
                    arrayDeque.offerFirst(i);
                }
            }
            result.add(arrayDeque.pollLast());
            if (!arrayDeque.isEmpty()) {
                indexStart = arrayDeque.getLast();
            }
        }
        String res = result.toString();
        return res.substring(1, res.length() - 1);
    }

    private boolean[][] matrix1() {
        boolean[][] matrix =
                {{false, true, true},
                        {true, false, true},
                        {true, true, false}};

        return matrix;
    }

    private boolean[][] matrix2() {
        boolean[][] matrix =
                {{false, false, true, false, false},
                        {false, false, false, true, false},
                        {true, false, false, true, true},
                        {false, true, true, false, false},
                        {false, false, true, false, false}};

        return matrix;
    }
}
