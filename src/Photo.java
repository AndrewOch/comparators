public class Photo implements Comparable<Photo> {

    private Integer dpi;
    private String title;
    private String date;
    private String album;

    public Photo(Integer dpi, String title, String date, String album) {
        this.dpi = dpi;
        this.title = title;
        this.date = date;
        this.album = album;
    }

    public String getTitle() {
        return title;
    }

    public Integer getDpi() {
        return dpi;
    }

    public String getDate() {
        return date;
    }

    public String getAlbum() {
        return album;
    }

    @Override
    public String toString() {
        return "\n" + album + " : " + title + ", date: " + date + " ; " + dpi + " dpi";
    }

    @Override
    public int compareTo(Photo o) {
        return this.title.compareTo(o.title);
    }

    @Override
    public boolean equals(Object o) {
        Photo photo = (Photo) o;
        return this.title.equals(photo.title);
    }

    @Override
    public int hashCode() {
        return title.hashCode();
    }
}
