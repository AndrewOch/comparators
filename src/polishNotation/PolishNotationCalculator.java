package polishNotation;

import java.util.ArrayDeque;

public class PolishNotationCalculator {

    public static void main(String[] args) {
        PolishNotationCalculator polishNotationCalculator = new PolishNotationCalculator();
        System.out.println("1 2 3 + * = " + polishNotationCalculator.reverseCalculator("1 2 3 + *") + "  (5 right)");
        System.out.println("3 5 + 2 * 6 7 + 8 9 - / - = " + polishNotationCalculator.reverseCalculator("3 5 + 2 * 6 7 + 8 9 - / -") + "   (29 right)");
        System.out.println("1 2 + * = " + polishNotationCalculator.reverseCalculator("1 2 + *") + "  (5 right)");
    }

    private Long reverseCalculator(String s) throws IllegalArgumentException {
        ArrayDeque<Long> arrayDeque = new ArrayDeque<>();
        Long l2;
        while (!s.isEmpty()) {
            switch (s.charAt(0)) {

                case '+':
                    l2 = arrayDeque.pollFirst();
                    try {
                        arrayDeque.offerFirst(l2 + arrayDeque.pollFirst());
                    } catch (NullPointerException e) {
                        throw new IllegalArgumentException();
                    }
                    break;
                case '-':

                    l2 = arrayDeque.pollFirst();
                    try {
                        arrayDeque.offerFirst(l2 - arrayDeque.pollFirst());
                    } catch (NullPointerException e) {
                        throw new IllegalArgumentException();
                    }
                    break;
                case '*':
                    l2 = arrayDeque.pollFirst();
                    try {
                        arrayDeque.offerFirst(l2 * arrayDeque.pollFirst());
                    } catch (NullPointerException e) {
                        throw new IllegalArgumentException();
                    }
                    break;
                case '/':

                    l2 = arrayDeque.pollFirst();
                    try {
                        arrayDeque.offerFirst(l2 / arrayDeque.pollFirst());
                    } catch (NullPointerException e) {
                        throw new IllegalArgumentException();
                    }

                    break;
                default:
                    arrayDeque.offerFirst(Long.parseLong(s.substring(0, s.indexOf(' '))));
                    break;
            }
            s = s.substring(1);
            if (s.length() > 1 && s.charAt(0) == ' ') {
                s = s.substring(1);
            } else {
                return arrayDeque.pollFirst();
            }
        }
        return null;
    }

}