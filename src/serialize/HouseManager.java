package serialize;

import java.io.*;
import java.util.ArrayList;

public class HouseManager {

    public static void main(String[] args) {
        Window window = new Window(4, "red");
        House house = new House(window, 4);
        House house2 = new House(window, 6);
        House house3 = new House(new Window(6, "blue"), 8);
        File file1 = new File("*\\src\\serialize\\text.txt");
        HouseManager houseManager = new HouseManager();
        houseManager.createFile(file1);

        ObjectOutputStream objectOutputStream = null;
        try {
            FileOutputStream fileOutputStream = new FileOutputStream("houses.txt");
            objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(house);
            objectOutputStream.close();
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileInputStream fileInputStream = null;
        try {
            fileInputStream = new FileInputStream("houses.txt");
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            House house1 = (House) objectInputStream.readObject();
            System.out.println(house1);
            fileInputStream.close();
            objectInputStream.close();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        File file = new File("HousesFile.txt");


        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter("houses1.txt");
            fileWriter.write(house.toString() + "\n");
            fileWriter.write(house2.toString() + "\n");
            fileWriter.write(house3.toString() + "\n");
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            FileReader fileReader = new FileReader("houses1.txt");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String str = bufferedReader.readLine();
            System.out.println(str);
            //house = new House(str.substring(str.indexOf('{'), (str.indexOf(',') - str.indexOf('{'))), str.substring(str.indexOf(','), (str.indexOf('}') - str.indexOf(','))));
            bufferedReader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    void createFile(File file) {
        if (file.isFile()) {

            try {
                FileWriter fileWriter = new FileWriter(file);
                System.out.println("File");
                fileWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (file.isDirectory()) {
            try {
                FileWriter fileWriter = new FileWriter(file);
                System.out.println("Dir");
                fileWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
}