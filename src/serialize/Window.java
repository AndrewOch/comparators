package serialize;

import java.io.Serializable;

public class Window implements Serializable {

    int squares;
    String color;

    public Window(int squares, String color) {
        this.squares = squares;
        this.color = color;
    }
}
