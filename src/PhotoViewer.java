import java.util.*;

public class PhotoViewer {

    ArrayList<Photo> photos = new ArrayList<>();

    public static void main(String[] args) {
        PhotoViewer photoViewer = new PhotoViewer();
        //photoViewer.run();
        photoViewer.run1();

    }

    private void run1() {
        photos.add(new Photo(300, "Sea", "1998/7/12", "family"));
        photos.add(new Photo(200, "Contract", "2010/5/3", "documents"));
        photos.add(new Photo(150, "Carl", "2015/10/6", "memes"));
        photos.add(new Photo(600, "Tenet", "2020/2/2", "posters"));
        photos.add(new Photo(600, "Tenet", "2020/2/2", "posters"));
        photos.add(new Photo(600, "Tenet", "2020/2/2", "posters"));
        photos.add(new Photo(600, "Tenet", "2020/2/2", "posters"));
        photos.add(new Photo(600, "Tenet", "2020/2/2", "posters"));
        photos.add(new Photo(300, "Retiring", "2018/8/4", "documents"));

        Collections.sort(photos);
        System.out.println("\nPhotos by title:");
        System.out.println(photos);

        HashSet<Photo> photoSet = new HashSet<Photo>(photos);
        System.out.println(photoSet);

        TreeSet<Photo> treeSet = new TreeSet<Photo>(photos);
        System.out.println("\nPhotos by title:");
        System.out.println(treeSet);

        System.out.println("\nPhotos by album:");
        treeSet = new TreeSet<Photo>(new PhotoByAlbum());
        treeSet.addAll(photos);
        System.out.println(treeSet);

        System.out.println("\nPhotos by dpi");
        treeSet = new TreeSet<Photo>(new PhotoByDpi());
        treeSet.addAll(photos);
        System.out.println(treeSet);

        System.out.println("\nPhotos by date");
        treeSet = new TreeSet<Photo>(new PhotoByDate());
        treeSet.addAll(photos);
        System.out.println(treeSet);
    }

    private void run() {
        photos.add(new Photo(300, "Sea", "12/7/1998", "family"));
        photos.add(new Photo(200, "Contract", "3/5/2010", "documents"));
        photos.add(new Photo(150, "Carl", "6/10/2015", "memes"));
        photos.add(new Photo(600, "Tenet", "2/2/2020", "posters"));
        photos.add(new Photo(300, "Retiring", "4/8/2018", "documents"));

        Collections.sort(photos);
        System.out.println("Photos by title:");
        System.out.println(photos);

        Collections.sort(photos, new PhotoByDate());
        System.out.println("Photos by date:");
        System.out.println(photos);

        Collections.sort(photos, new PhotoByAlbum());
        System.out.println("Photos by album:");
        System.out.println(photos);

        Collections.sort(photos, new PhotoByDpi());
        System.out.println("Photos by dpi:");
        System.out.println(photos);


    }

    class PhotoByAlbum implements Comparator<Photo> {

        @Override
        public int compare(Photo o1, Photo o2) {

            int result = o1.getAlbum().compareTo(o2.getAlbum());
            if (result == 0) {

                return o1.getTitle().compareTo(o2.getTitle());
            }
            return result;
        }

    }

    class PhotoByDate implements Comparator<Photo> {

        @Override
        public int compare(Photo o1, Photo o2) {
            int result = o1.getDate().compareTo(o2.getDate());

            if (result == 0) {
                return o1.getTitle().compareTo(o2.getTitle());
            }
            return result;
        }
    }

    class PhotoByDpi implements Comparator<Photo> {

        @Override
        public int compare(Photo o1, Photo o2) {

            int result = o1.getDpi().compareTo(o2.getDpi());

            if (result == 0) {
                return o1.getTitle().compareTo(o2.getTitle());
            }
            return result;
        }
    }

}


