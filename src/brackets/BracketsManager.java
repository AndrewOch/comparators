package brackets;

import java.util.ArrayDeque;


public class BracketsManager {

    public static void main(String[] args) {
        BracketsManager bracketsManager = new BracketsManager();
        System.out.println("((fred{rv})[])  :  " + bracketsManager.checkValidity("((fegg{rv})[])"));
        System.out.println("}((1245{rv})[]) :  " + bracketsManager.checkValidity("}((1245{rv})[])"));
        System.out.println("34344 :  " + bracketsManager.checkValidity("34344"));
    }

    private boolean checkValidity(String s) {
        if (s.equals("")) {
            return true;
        }
        ArrayDeque<Character> brackets = new ArrayDeque<>();
        for (int i = 0; i < s.length(); i++) {
            switch (s.charAt(i)) {
                case '(':
                    brackets.offerFirst('(');
                    break;

                case '[':
                    brackets.offerFirst('[');
                    break;
                case '{':
                    brackets.offerFirst('{');
                    break;
                case ')':
                    if (!brackets.isEmpty() && brackets.getFirst() == '(') {
                        brackets.removeFirst();
                    } else return false;
                    break;

                case ']':
                    if (!brackets.isEmpty() && brackets.getFirst() == '[') {
                        brackets.removeFirst();
                    } else return false;
                    break;

                case '}':
                    if (!brackets.isEmpty() && brackets.getFirst() == '{') {
                        brackets.removeFirst();
                    } else return false;

                    break;

            }
        }

        return brackets.isEmpty();
    }
}
